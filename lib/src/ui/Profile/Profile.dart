import 'package:flutter/material.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class Profile extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          "Mi Perfil",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: colorAccent,
      ),
      body: Stack(
        children: <Widget>[dashBg, content],
      ),
    );
  }

  get dashBg => Column(
    children: <Widget>[
      Expanded(
        child: Container(color: colorAccent),
        flex: 2,
      ),
      Expanded(
        child: Container(color: Colors.transparent),
        flex: 5,
      ),
    ],
  );

  get content => Container(
    child: Column(
      children: <Widget>[
        grid
      ],
    ),
  );

  get grid => Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.verified_user, color: Colors.white,),
                  ),
                  title: Text('Estado de Usuario',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Verificado',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.lock, color: Colors.white,),
                  ),
                  title: Text('Opcion de contraseña',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Cambiar',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                     backgroundColor: colorPrimary,
                    child: Icon(Icons.phone, color: Colors.white,),
                  ),
                  title: Text('Teléfono asociado',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('000 0000 000',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.person, color: Colors.white,),
                  ),
                  title: Text('ID Usuario',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('00000',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
            ],
          )
        ),
      );
}