import 'package:convert/convert.dart';
import 'package:flutter_blue/flutter_blue.dart';
//import 'package:flutter_blue/flutter_blue.dart';
import 'package:remotelockapp/src/resources/RemoteSecureService.dart';

class RemoteCoreService{
  BluetoothDevice _device;
  var _mac;
  int _length = 36;
  var _frame_mask = '4D'; // 'M' Define el caracter para enmascaramiento de la trama
  var _command_header = '23'; // '#' Define el encabezado para tramas de comandos u ordenes.
  var _electrical_header = '2A'; // '*'  Define el encabezado para envio de trama de datos electricos
  var _watch_header = '25'; //  '%' define el encabezado para una trama de informacion de datos del Real Time Clock
  var _text_header = '40'; // '@'  Define el encabezado para una trama de envio de texto
  var _info_header = '24'; // '$' Define el encabezado para envio de tramas de informacion
  var _inquiry_header = '3F'; // '?' Define el encabezado para tramas de peticion de informacion
  var _inc_dec;
  var _inc_unit;
  var _res_dec;
  var _res_unit;
  //Trama Bluetooth
  List<String> _trama;
  //Comandos
  var open = '3C'; // Comando <
  var close = '3E'; // Comando >
  RemoteSecureService secureService = new RemoteSecureService();


  RemoteCoreService(BluetoothDevice device){
    _device = device;
    initTrama();
  }

  initTrama(){
    resetTrama();
  }

  resetTrama() {
    _trama = new List(_length);
    for(int i = 0; i < _length; i++ ){
        _trama[i] = '78';
    } 
  } 

  accionTrama(int pos, bool action, String contador, String incremental){
    resetTrama();
     var macSplit = _device.id.toString().split(':');
     var comando = "";
    _mac = secureService.toCharCode(macSplit.join());
    //_mac = secureService.toListInteger(_mac);
    List<String> incChar = secureService.incrementNumber(int.parse(incremental) + 3);
    List<String> resChar = secureService.incrementNumber(int.parse(contador));

    if(!action){
      comando = negarComando("3E");
    }else{
      comando = negarComando("3C");
    }

     _trama[0] = _command_header;
    _trama[1] = '34';
    _trama[2] = '31';
    _trama[4] = comando;
    _trama[5] = '3' + pos.toString();
    _trama[10] = '3'+ incChar[0].toString();
    _trama[11] = '3'+ incChar[1].toString();
    _trama[12] = stringtoHex(_mac[0].toString());
    _trama[13] = stringtoHex(_mac[1].toString());
    _trama[14] = '3A';
    _trama[15] = stringtoHex(_mac[2].toString());
    _trama[16] = stringtoHex(_mac[3].toString());
    _trama[17] = '3A';
    _trama[18] = stringtoHex(_mac[4].toString());
    _trama[19] = stringtoHex(_mac[5].toString());
    _trama[20] = '3A';
    _trama[21] = stringtoHex(_mac[6].toString());
    _trama[22] = stringtoHex(_mac[7].toString());
    _trama[23] = '3A';
    _trama[24] = stringtoHex(_mac[8].toString());
    _trama[25] = stringtoHex(_mac[9].toString());
    _trama[26] = '3A';
    _trama[27] = stringtoHex(_mac[10].toString());
    _trama[28] = stringtoHex(_mac[11].toString());
    _trama[29] = '3'+ resChar[0].toString();
    _trama[30] = '3'+ resChar[1].toString();
    _trama[34] = '0D';
    _trama[35] = '0A';

    _inc_dec = incChar[0];
    _inc_unit = incChar[1];

    print("---------------------> Resultado");
    print(hex.decode(_trama.join()));

    return hex.decode(_trama.join());

  }

  syncupTrama(List<int> trama, int pos){
    var macSplit = _device.id.toString().split(':');
    _mac = secureService.toCharCode(macSplit.join());
    _inc_dec = secureService.toStringChar([trama[12]]);
    _inc_unit = secureService.toStringChar([trama[13]]);
    _res_dec =  secureService.toStringChar([trama[29]]);
    _res_unit =  secureService.toStringChar([trama[30]]);
    //_mac = secureService.toListInteger(_mac);

    List<String> incChar = secureService.incrementNumber(int.parse(_inc_dec + _inc_unit));
    List<String> resChar = secureService.incrementNumber(int.parse(_res_dec + _res_unit));

    _trama[0] = _command_header;
    _trama[1] = '34';
    _trama[2] = '31';
    _trama[4] = '3' + pos.toString();
    _trama[10] = '3'+ incChar[0].toString();
    _trama[11] = '3'+ incChar[1].toString();
    _trama[12] = stringtoHex(_mac[0].toString());
    _trama[13] = stringtoHex(_mac[1].toString());
    _trama[14] = '3A';
    _trama[15] = stringtoHex(_mac[2].toString());
    _trama[16] = stringtoHex(_mac[3].toString());
    _trama[17] = '3A';
    _trama[18] = stringtoHex(_mac[4].toString());
    _trama[19] = stringtoHex(_mac[5].toString());
    _trama[20] = '3A';
    _trama[21] = stringtoHex(_mac[6].toString());
    _trama[22] = stringtoHex(_mac[7].toString());
    _trama[23] = '3A';
    _trama[24] = stringtoHex(_mac[8].toString());
    _trama[25] = stringtoHex(_mac[9].toString());
    _trama[26] = '3A';
    _trama[27] = stringtoHex(_mac[10].toString());
    _trama[28] = stringtoHex(_mac[11].toString());
    _trama[29] = '3'+ resChar[0].toString();
    _trama[30] = '3'+ resChar[1].toString();
    _trama[34] = '0D';
    _trama[35] = '0A';

    print("---------------------> Resultado");
    print(hex.decode(_trama.join()));
   
    return hex.decode(_trama.join());

  }

  negarComando(String comando){
    List<int> decimalData = hex.decode(comando);
    int negacionData = ~decimalData[0];
    negacionData = negacionData.toUnsigned(8);
    print("----------------> Comando negado");
    print(hex.encode([negacionData]));
    print(negacionData);
    return hex.encode([negacionData]);

  }

  stringtoHex(String data){
      var ascii = data.codeUnitAt(0);
      var hexa = hex.encode([ascii]);
      return hexa;
  }

}