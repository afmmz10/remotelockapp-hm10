import 'dart:math';
import 'package:convert/convert.dart';

class RemoteSecureService{

  List<int> desencryptTrama(List<int> tramaEnc) {
    int sumBinary = 0;
    //Realiza un suma desde la posicion 0 hasta la 31
    for(int i = 0; i < tramaEnc.length ; i++){
      if(i < 32){
        sumBinary = sumBinary + tramaEnc[i];
      }else{
        break;
      }
    }
    sumBinary = sumBinary.toUnsigned(16);
    int negacionSum = 65535 - sumBinary;
    String hexsum = negacionSum.toRadixString(16);
    String checksum = "";
    if(tramaEnc[33] < 16){
      checksum = tramaEnc[32].toRadixString(16).toString() + "0" + tramaEnc[33].toRadixString(16).toString();
    }else{
      checksum = tramaEnc[32].toRadixString(16).toString() + tramaEnc[33].toRadixString(16).toString();
    }
    //Valida si el cheksum son iguales
    if(checksum.contains(hexsum)){  
      //Se realiza un XOR con el caracter M = 60
      for(int i = 0; i < tramaEnc.length ; i++){
        if(i < 32){
          tramaEnc[i] = tramaEnc[i].toUnsigned(8) ^ 60.toUnsigned(8);
        }else{
          break;
        }
      }
      //Segundo XOR con valor de la posicion 31
      for(int i = 0; i < tramaEnc.length ; i++){
        if(i < 31){
          tramaEnc[i] = tramaEnc[i].toUnsigned(8) ^ tramaEnc[31].toUnsigned(8);
        }else{
          break;
        }
      }
      return tramaEnc;
    }
    return [];
  }

  incrementNumber(int number){
    List<String> returnData = [];
    if(number > 99){
      number = 3;
    }
    if(number < 10){
      returnData = toCharCode('0' + number.toString());
    }else{
      returnData = toCharCode(number.toString());
    }
    return returnData;

  }

  encrypTrama(List<int> tramaEnc){
    Random rnd;
    int min = 1;
    int max = 255;
    rnd = new Random();
    int random = min + rnd.nextInt(max - min);
    int sumBinary = 0;
    tramaEnc[31] = random;

    for(int i = 0; i < tramaEnc.length ; i++){
      if(i < 31){
        tramaEnc[i] = tramaEnc[i].toUnsigned(8) ^ tramaEnc[31].toUnsigned(8);
      }else{
        break;
      }
    }

    for(int i = 0; i < tramaEnc.length ; i++){
      if(i < 32){
        tramaEnc[i] = tramaEnc[i].toUnsigned(8) ^ 60.toUnsigned(8);
      }else{
        break;
      }
    }

    for(int i = 0; i < tramaEnc.length ; i++){
      if(i < 32){
        sumBinary = sumBinary + tramaEnc[i];
      }else{
        break;
      }
    }

    int negacionSum = 65535 - sumBinary;
    String hexsum = negacionSum.toRadixString(16);
    List<int> checksum = hex.decode(hexsum);

    tramaEnc[32] = checksum[1];
    tramaEnc[33] = checksum[0];

    return tramaEnc;
  }

  toStringChar(List<int> data){
    return String.fromCharCodes(data);
  }

  toCharCode(String trama){
    return trama.codeUnits.map((unit) {
      return new String.fromCharCode(unit);
    }).toList(); 
  }

  toListInteger(List<String> list){
    List<int> returnData = [];
    for(int i = 0; i < list.length; i++){
        returnData.add(int.parse(list[i]));
    }

    return returnData;
  }

}