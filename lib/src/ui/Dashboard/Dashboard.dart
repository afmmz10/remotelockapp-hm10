import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:remotelockapp/src/models/options.dart';
import 'package:remotelockapp/src/ui/About/About.dart';
import 'package:remotelockapp/src/ui/Options/Options.dart';
import 'package:remotelockapp/src/ui/Profile/Profile.dart';
import 'package:remotelockapp/src/ui/ble/ble_screen.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class SO extends StatefulWidget{
   _SOState createState() => _SOState();
}

class _SOState extends State<SO> {

  List<Options> listOptions = [
    new Options(name: "Remotes", icon: Icons.bluetooth, option: 1),
    new Options(name: "Perfil", icon: Icons.person, option: 2),
    new Options(name: "Opciones", icon: Icons.settings, option: 3),
    new Options(name: "Acerca de", icon: Icons.info, option: 4),
  ];

  var email = "N/A";

   @override
  void initState() {
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[dashBg, content],
      ),
    );
  }

  get dashBg => Column(
      children: <Widget>[
        Expanded(
          child: Container(color: colorAccent),
          flex: 2,
        ),
        Expanded(
          child: Container(color: Colors.transparent),
          flex: 5,
        ),
      ],
    );

  get content => Container(
        child: Column(
          children: <Widget>[
            header,
            grid,
          ],
        ),
      );

  get header => ListTile(
    contentPadding: EdgeInsets.only(left: 20, right: 20, top: 40),
    title: Text(
      'RemoteLock',
      style: TextStyle(color: Colors.white, fontSize: 18),
    ),
    subtitle: Text(
      'demoe@demo.com',
      style: TextStyle(color: Colors.white),
    ),
    trailing:  CircleAvatar(
      backgroundColor: Colors.white,
      backgroundImage: AssetImage('assets/images/avatar.png'),
    ),
  );

  get grid => Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: GridView.count(
            crossAxisSpacing: 16,
            mainAxisSpacing: 16,
            crossAxisCount: 2,
            childAspectRatio: .90,
            children: List.generate(listOptions.length, (index) {
              return Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: InkWell(
                    onTap: () {
                      switch (listOptions[index].option) {
                        case 1:
                           Navigator.push(context, MaterialPageRoute(builder: (context) => BleScreen(user: email,) ));
                          break;
                        case 2:
                           //Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));
                          break;
                        case 3:
                           //Navigator.push(context, MaterialPageRoute(builder: (context) => OptionsScreen()));
                          break;
                        case 4:
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => About()));
                          break;
                        default:
                      }
                    },
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            listOptions[index].icon,
                            color: colorAccent,
                            size: 40.0,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(listOptions[index].name,
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                          )
                        ],
                    ),
                  ),
                )
              );
            }),
          ),
        ),
      );
}