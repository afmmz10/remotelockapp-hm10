
class Constants{  
  static String appName = "Remote Lock";
  static String logCollection = "Log";
  static String devicesCollection = "Devices";
  static String deviceEnroll = "DeviceEnroll";
  static String deviceHistory = "DeviceHistory";
  static String onBoardIntro = "OnBoardIntro";
  static String contadorkey = "countDevice";
}
