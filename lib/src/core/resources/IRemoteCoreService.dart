abstract class IRemoteCoreService {

  Future<void> encodeTrama();

  Future<void> generateTrama();

}