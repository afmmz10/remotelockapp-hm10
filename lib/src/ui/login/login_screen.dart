import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remotelockapp/src/core/resources/IFirebaseAuthService.dart';

import 'login_demo.dart';

class LoginScreen extends StatelessWidget {
  final IFirebaseAuthService _authService;

   LoginScreen({Key key, @required IFirebaseAuthService authService})
      : assert(authService != null),
        _authService = authService,
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: LoginDemo()
      );
  }
}