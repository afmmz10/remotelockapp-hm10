import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:remotelockapp/src/ui/ble/views/ble_device.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'package:remotelockapp/src/utils/Validators.dart';

class FindDevicesScreen extends StatelessWidget {
  final streamDevices = Stream.periodic(Duration(seconds: 2)).asyncMap((_) => FlutterBlue.instance.connectedDevices).asBroadcastStream();
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2, 
        child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            "Remote",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.white
          ),
          backgroundColor: colorAccent,
          brightness: Brightness.dark,
          bottom: TabBar(
            tabs: <Widget>[
              Tab(icon: Icon(Icons.bluetooth_connected),
              text: 'Conexiones activas',),
              Tab(icon: Icon(Icons.bluetooth_searching),
              text: 'Buscar Dispositivo',),
            ]
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                   StreamBuilder<List<BluetoothDevice>>(
                    stream: streamDevices,
                    initialData: [],
                    builder: (c, snapshot) => Column(
                      children: snapshot.data
                          .map((d) => GestureDetector(
                            onTap: (){
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) {
                                  return BleAdmin(device: d);
                              }));
                            },
                            child: Card(
                                child: ListTile(
                                  leading: Icon(Icons.bluetooth_connected, color: colorPrimary,),
                                  title: Text(
                                    d.name.isEmpty ? 'Undefined' : d.name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  subtitle: Text(
                                    d.id.toString(),
                                    style: Theme.of(context).textTheme.caption,
                                  ),
                                ),
                              ),
                          ))
                          .toList(),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  StreamBuilder<List<ScanResult>>(
                    stream: FlutterBlue.instance.scanResults,
                    initialData: [],
                    builder: (c, snapshot) => Column(
                      children: snapshot.data
                          .map(
                            (r) => GestureDetector(
                              onTap: () => showModalBottomSheet(
                                  context: context,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  backgroundColor: Colors.white,
                                  builder: (context){
                                    return new Container(
                                      height: (MediaQuery.of(context).size.height * 35) / 100,
                                      color: Colors.transparent,
                                      child: new Container(
                                        decoration: new BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(height: 20,),
                                            Text('Confirmar conexión a unidad', style: TextStyle(fontSize: 20),),
                                            SizedBox(height: 10,),
                                            ListTile(
                                              leading: CircleAvatar(
                                                backgroundColor: colorPrimary,
                                                child: Icon(Icons.devices, color: Colors.white,),
                                              ),
                                              title: Text(r.device.name,style: TextStyle(
                                                  color: Colors.black
                                              ),),
                                              subtitle: Text(r.device.id.toString(),style: TextStyle(
                                                  color: Colors.grey
                                              ),),
                                            ),
                                            Divider(),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new FlatButton(
                                                  padding: const EdgeInsets.all(8.0),
                                                  textColor: colorPrimary,
                                                  color: Colors.white,
                                                  onPressed: (){
                                                    Navigator.pop(context);
                                                    Navigator.of(context)
                                                        .push(MaterialPageRoute(builder: (context) {
                                                        r.device.connect();
                                                        return BleAdmin(device: r.device);
                                                    }));
                                                    
                                                  },
                                                  child: new Text("CONECTARME"),
                                                ),
                                                new FlatButton(
                                                  onPressed: () => Navigator.pop(context),
                                                  textColor: colorError,
                                                  color: Colors.white,
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: new Text(
                                                    "CANCELAR",
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                    ));
                                  }
                              ),
                              child: Card(
                                child: ListTile(
                                  leading: Icon(Icons.wifi, color: Validators.rssiRange(r.rssi),),
                                  title: Text(
                                    r.device.name.isEmpty ? 'Undefined' : r.device.name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  subtitle: Text(
                                    r.device.id.toString(),
                                    style: Theme.of(context).textTheme.caption,
                                  ),
                                ),
                              ),
                            )
                          )
                          .toList(),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        floatingActionButton: StreamBuilder<bool>(
          stream: FlutterBlue.instance.isScanning,
          initialData: false,
          builder: (c, snapshot) {
            if (snapshot.data) {
              return FloatingActionButton(
                child: Icon(Icons.stop , color: Colors.white),
                onPressed: () => FlutterBlue.instance.stopScan(),
                backgroundColor: Colors.redAccent,
              );
            } else {
              return FloatingActionButton(
                  child: Icon(Icons.search, color: Colors.white),
                  onPressed: () => FlutterBlue.instance
                      .startScan(timeout: Duration(seconds: 4)));
            }
          },
        ),
      )
    );
  }
}
