import 'package:flutter/material.dart';

class Validators{
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  static final RegExp _passwordRegExp = RegExp(
    r'^[a-zA-Z0-9]{8,}$',
  );
  
 static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }

  static rssiRange(int range){
    if (range >= -35) return Colors.greenAccent[700];
    else if (range >= -45) return Color.lerp(Colors.greenAccent[700], Colors.lightGreen,        -(range + 35) / 10);
    else if (range >= -55) return Color.lerp(Colors.lightGreen,       Colors.lime[600],         -(range + 45) / 10);
    else if (range >= -65) return Color.lerp(Colors.lime[600],        Colors.amber,             -(range + 55) / 10);
    else if (range >= -75) return Color.lerp(Colors.amber,            Colors.deepOrangeAccent,  -(range + 65) / 10);
    else if (range >= -85) return Color.lerp(Colors.deepOrangeAccent, Colors.redAccent,         -(range + 75) / 10);
    else return Colors.redAccent;
  }

  static dateValid(){
    return DateTime.now().toString();
  }
}