import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remotelockapp/src/SimpleBlocDelegate.dart';
import 'package:remotelockapp/src/resources/StorageService.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'src/app.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  await StorageUtil.getInstance();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: colorPrimary,
    statusBarColor: colorPrimary,
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) {
      runApp(
        App()
        /*BlocProvider(
          create: (context) => AuthBloc(
          // authService: authService,
          )..add(AppStart()),
          child: App()//authService: authService),
        ),*/
      );
    });
  
}