import 'package:flutter/material.dart';

class Options{
  String name;
  IconData icon;
  int option;
  
  Options({this.name, this.icon, this.option});
}