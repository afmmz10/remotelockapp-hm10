import 'package:flutter/material.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image(
                  image: AssetImage('assets/images/Logo-app.jpg'),
                  height: 250,
                ),
                Container(
                    margin: EdgeInsets.only(top: 32, bottom: 16),
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.white,
                      valueColor: new AlwaysStoppedAnimation<Color>(colorPrimary),
                    ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}