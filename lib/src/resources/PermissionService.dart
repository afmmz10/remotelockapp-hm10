import 'package:permission_handler/permission_handler.dart';

class PermissionsService {

  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {
    var permission = Permission.location;
    var result = await permission.request();
    if (result != PermissionStatus.granted) {
        onPermissionDenied();
    }
    return true;
  }

  Future<bool> statuslocationPermission() async{
     var permission = Permission.location;
    var result = await permission.request();
    if (result != PermissionStatus.granted) {
        return false;
    }
    return true;
  }

}