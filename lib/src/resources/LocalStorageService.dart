import 'package:shared_preferences/shared_preferences.dart';

class SharedRepository{
  Future<String> getData(String key) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<void> removeData(String key) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }

  Future<void> saveData(String data, String key) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, data);
  }

  Future<bool> thereData(String key) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

}