import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:remotelockapp/src/resources/PermissionService.dart';
import 'package:remotelockapp/src/resources/StorageService.dart';
import 'package:remotelockapp/src/ui/Welcome/Welcome.dart';
import 'package:remotelockapp/src/ui/login/login_screen.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'package:remotelockapp/src/utils/const.dart';

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    bool state = false;
    PermissionsService().statuslocationPermission().then((value) => state = value);
    if(!state){
        Flushbar(
          margin: EdgeInsets.all(8),
          message: "Debe configurar los permisos en la sección de opciones",
          icon: Icon(
            Icons.warning,
            size: 28.0,
            color: Colors.orange,
            ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.orange,
        )..show(context);
    }
    StorageUtil.putBool(Constants.onBoardIntro, true);
    Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(builder: (context) => WelcomePage()),
      (Route<dynamic> route) => false);

  }

  Widget _buildImage(String assetName, String extension) {
    return Align(
      child: Image.asset('assets/images/$assetName.$extension', width: 200.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: "RemoteLock",
          body:"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
          image: _buildImage('Logo-app', 'jpg' ),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Permisos",
          body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
          image: _buildImage('Logo-app', 'jpg' ),
          /*footer: RaisedButton(
            onPressed: () {
                PermissionsService().requestLocationPermission(
                  onPermissionDenied: () {
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      message: "Se han negado los permisos necesarios para continuar",
                      icon: Icon(
                        Icons.error,
                        size: 28.0,
                        color: Colors.redAccent,
                        ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.redAccent,
                    )..show(context);
                }).then((value) => (){
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      message: "Se ha concedido los permisos necesarios",
                      icon: Icon(
                        Icons.check,
                        size: 28.0,
                        color: Colors.greenAccent,
                        ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.greenAccent,
                    )..show(context);
                });
            },
            child: const Text(
              'Conceder permisos',
              style: TextStyle(color: Colors.white),
            ),
            color: colorAccent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),*/
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text('OMITIR'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('OK', style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeColor:  Color(0xFF098c80),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}