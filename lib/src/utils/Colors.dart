import 'dart:ui';

final Color colorPrimaryDark = Color(0xFF098c80);
final Color colorPrimary = Color(0xFF00a191);
final Color colorAccent = Color(0xFF0ab4a5);
final Color colorActive = Color(0xFF729D39);
final Color colorError = Color(0xFFC10000);