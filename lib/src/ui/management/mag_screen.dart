import 'dart:async';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_blue/flutter_blue.dart';
import 'package:remotelockapp/src/models/logDevice.dart';
import 'package:remotelockapp/src/resources/RemoteCoreService.dart';
import 'package:remotelockapp/src/resources/RemoteSecureService.dart';
import 'package:remotelockapp/src/ui/management/widgets/actionPulse.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class MagScreen extends StatefulWidget {

  //final BluetoothDevice device;
  //MagScreen({Key key, this.device}) : super(key: key);
  _MagScreenState createState() => _MagScreenState();
}


class _MagScreenState extends State<MagScreen> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }

 /* BluetoothDevice _device;
  FlutterBlue flutterBlue = FlutterBlue.instance;
  List<BluetoothService> _services;
  bool isConnected = true;
  bool stateConnection = false;
  BluetoothCharacteristic characteristic;
  List<int> _buffer = [];
  RemoteSecureService secureService = new RemoteSecureService();
  AnimationController _controller;
  LogDevice _logDevice;
  String contador = "00";
  String incremental = "00";

  @override
  void initState() {
    super.initState();
    _device = this.widget.device;
    _controller = new AnimationController(
      vsync: this,
    );
    connect(widget.device); 
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async{
    debugPrint('estado -> ${state.toString()}');
    if(state == AppLifecycleState.paused){
      await widget.device.disconnect();
    } 
  }

  @override
  void dispose() async{
    super.dispose();
     _controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    await widget.device.disconnect();
  }

  void _stopAnimation(){
    _controller.stop();
  }

  void _startAnimation() {
    _controller.stop();
    _controller.reset();
    _controller.repeat(
      period: Duration(seconds: 1),
    );
  }

  bool areListsEqual(var list1, var list2) {
    // check if both are lists
    if(!(list1 is List && list2 is List)
        // check if both have same length
        || list1.length!=list2.length) {
        return false;
    }
    
    // check if elements are equal
    for(int i=0;i<list1.length;i++) {
        if(list1[i]!=list2[i]) {
            return false;
        }
    }
    return true;
}

  void changeStatusConnected(){
    setState(() {
      isConnected = !isConnected;
    });
  }

  void changeStatusConnection(){
    setState(() {
      stateConnection = !stateConnection;
    });
  }

  void processData(List<int> data) {

    RemoteCoreService corService = new RemoteCoreService(_device);
    
    data = secureService.desencryptTrama(data);

    debugPrint('Trama Valida: ${data.toString()}');

    if(data != null){

      switch (data[0]) {
        //Cuando es informacion
        case 36:
        bool state = false;
            String text = "";
            switch(data[12]){
              case 73:
                state = false;
                text = "Puerta Liberada";
                break;
              case 68:
                state = true;
                text = "Puerta Bloqueada";
                break;
              case 72:
                state = false;
                text = "Puerta Abierta";
                break;
              case 67:
                state = false;
                text = "Puerta Cerrada";
                break;
              case 84:
               state = false;
               text = "Actuador Forzado";
                break;
              case 74:
                state = false;
                text = "Actuador Desconectado";
                break;
              case 71:
                state = false;
                text = "Puerta en Riesgo";
                break;
              case 77:
                state = false;
                text = "Motor en Movimiento";
                break;
              case 78:
                state = true;
                text = "Intento de Apertura no Autorizada";
                break;
              case 79:
                state = true;
                text = "Apertura por Boton Interno";
                break;
              case 69:
                state = true;
                text = "Cierre por Boton Interno";
                break;
              default:
                state = false;
                break;
            }
          if(_logDevice == null){
            _logDevice = new LogDevice(name: widget.device.name, stateText: "", state: false, isSendData: false);
            setState(() {
              _logDevice.state = state;
              _logDevice.stateText = text;
            });
          }else{
            setState(() {
              _logDevice.state = state;
              _logDevice.stateText = text;
            });
          }
          break;
        //Cuando es sincronización
      case 115:
        List<int> dataTrama = corService.syncupTrama(data, 1);
        //almacenamos contador
        contador = String.fromCharCode(dataTrama[29]) + String.fromCharCode(dataTrama[30]);
        print("---------------------->" + contador);
        String incTemp = String.fromCharCode(data[12]) + String.fromCharCode(data[13]);
        int incTmp= int.parse(incTemp);
        incremental = incTmp.toString();
        print("---------------------->" + incremental);
        //sendMessage(secureService.encrypTrama(dataTrama));
        break;
      //Retorna incremental
      case 35:
        String incTemp = String.fromCharCode(data[1]) + String.fromCharCode(data[2]);
        int incTmp= int.parse(incTemp) - 3;
        incremental = incTmp.toString();
        //_setCommand(logCommand.pos, logCommand.value, logCommand.res, incremental);
        break;
        default:
      }

    }else{

    }

  }

  void onReceiveData(List<int> data) async{
    debugPrint('Data recibidad: ${data.toString()}');
    debugPrint('Long: ${data.length}');
    
    if(data.length > 0){
      data.forEach((byte) {
        _buffer.add(byte);
      });
      if(_buffer[_buffer.length - 1] == 10 && _buffer[_buffer.length - 2] == 13){
        List<int> bufferRep = []..addAll(_buffer);
        _buffer.clear();
        switch (bufferRep.length) {
          case 4:
            //Mensaje ok
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("OK")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "OK",
                icon: Icon(
                  Icons.check,
                  size: 28.0,
                  color: Colors.greenAccent,
                  ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.greenAccent,
              )..show(context);
            }
            break;
          case 7:
            //Mensjae error
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("ERROR")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                  ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);
            }
            break;
          case 11:
            //Estado del sync
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("syncupbad")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                  ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);  
            }else if(dataTrama.contains("syncup:ok")){
              //Almacena contador
              
            }
            break;
          case 36:
            //Procesa trama
            debugPrint('Debo procesar la trama');
            processData(bufferRep);
            break;
          default:
            break;
        }
      }
    }

  }

  void generateConfiguration(List<BluetoothService> services) async{
    var service = services.firstWhere((service) => service.toString().contains('0000ffe0'));
      if(service != null){
        characteristic = service.characteristics.firstWhere((ch) => ch.toString().contains('0000ffe1'));
        await characteristic.setNotifyValue(true);
        //Generamos escuchador de datos
        characteristic.value.listen((data) {
          onReceiveData(data);
        },);
        debugPrint('UUID : ${characteristic.uuid}');
      }else{
        debugPrint('Paila no hay servicios ni caracteristicas');
      }
  }
  
  Future connect(BluetoothDevice device) async {
    try {
      debugPrint('Me conecto....');
      await device.connect();
    } catch (e) {
      changeStatusConnected();
      debugPrint('No me conecto ${e.code}');
      if (e.code != 'already_connected') {
        throw e;
      }
    } finally {
      changeStatusConnected();
      changeStatusConnection();
      debugPrint("ya me conecte");
      _services = await device.discoverServices();
      generateConfiguration(_services);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.device.name,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: colorAccent,
        brightness: Brightness.dark,
      ),
      body: Center(
        child: isConnected ? CircularProgressIndicator() : Container(
          color: Colors.white,
          alignment: AlignmentDirectional(0.0, 0.0),
          child: Container(
            color: Colors.white,
            margin: new EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 10.0
            ),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 2),
                CustomPaint(
                  painter: new SpritePainter(_controller),
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                  ),
                ),
                SizedBox(height: 2),
                SettingsCard()
              ],
            ),
        ),
        ),
      )
    );
  }*/
}