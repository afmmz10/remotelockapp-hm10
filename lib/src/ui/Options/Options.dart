import 'package:flutter/material.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class OptionsScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          "Opciones",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: colorAccent,
      ),
      body: Stack(
        children: <Widget>[dashBg, content],
      ),
    );
  }

  get dashBg => Column(
    children: <Widget>[
      Expanded(
        child: Container(color: colorAccent),
        flex: 2,
      ),
      Expanded(
        child: Container(color: Colors.transparent),
        flex: 5,
      ),
    ],
  );

  get content => Container(
    child: Column(
      children: <Widget>[
        grid
      ],
    ),
  );

  get grid => Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.bluetooth, color: Colors.white,),
                  ),
                  title: Text('Estado de Bluetooth',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                     backgroundColor: colorPrimary,
                    child: Icon(Icons.notifications, color: Colors.white,),
                  ),
                  title: Text('Estado Notificaciones',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Inactivo',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.help, color: Colors.white,),
                  ),
                  title: Text('Preguntas Frecuentes',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Necesito ayuda',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.exit_to_app, color: Colors.white,),
                  ),
                  title: Text('Cerrar sesión',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Salir',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    
                  },
                ),
              ),
            ],
          )
        ),
      );

}