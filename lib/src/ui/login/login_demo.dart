
import 'package:flutter/material.dart';
import 'package:remotelockapp/src/ui/Dashboard/Dashboard.dart';
import 'package:remotelockapp/src/ui/Widgets/bezierContainer.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class LoginDemo extends StatefulWidget {
  State<LoginDemo> createState() => _LoginDemo();
}

class _LoginDemo extends State<LoginDemo> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: height,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: -height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: ListView(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(height: height * .2),
                            Image(
                              image: AssetImage(
                                  'assets/images/logo_app.png'),
                              height: 100,
                            ),
                            SizedBox(height: 30),
                            GestureDetector(
                            onTap: (){
                              Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (context) => SO()),
                                (Route<dynamic> route) => false);
                            },
                            child: Container(
                              width:
                                  MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  vertical: 15),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(5)
                                  ),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey.shade200,
                                        offset: Offset(2, 4),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ],
                                  gradient: LinearGradient(
                                    begin:
                                        Alignment.centerLeft,
                                    end:
                                        Alignment.centerRight,
                                    colors: [
                                        colorPrimary,
                                        colorPrimaryDark
                                      ])
                              ),
                              child: Text(
                                'Iniciar demo',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                            )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ]
            )
          )
        ],
      ),
    );
  }
}