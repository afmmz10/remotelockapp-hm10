import 'package:equatable/equatable.dart';

class Log extends Equatable {
  final String action;
  final String data;
  final String email;
  final String date;

  Log({this.action, this.data, this.email, this.date});

  @override
  // instead of doing super for equatable, we are doing this.
  List<Object> get props => [this.action, this.data, this.email, this.date];

}