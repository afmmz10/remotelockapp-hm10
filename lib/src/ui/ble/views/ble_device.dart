import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:remotelockapp/src/models/deviceHistory.dart';
import 'package:remotelockapp/src/models/log_model.dart';
import 'package:remotelockapp/src/resources/LocalStorageService.dart';
import 'package:remotelockapp/src/resources/RemoteCoreService.dart';
import 'package:remotelockapp/src/resources/RemoteSecureService.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'package:remotelockapp/src/utils/const.dart';

class BleAdmin extends StatefulWidget {
  final BluetoothDevice device;
  BleAdmin({Key key, this.device}) : super(key: key);
  BleAdminState createState() => BleAdminState();
}

class BleAdminState extends State<BleAdmin> with WidgetsBindingObserver {
  BluetoothDevice device;
  RemoteSecureService secureService = new RemoteSecureService();
  SharedRepository sharedPreference = new SharedRepository();
  BluetoothDeviceState state;

  bool statusSync = false;
  List<int> _buffer = [];
  String contador = "00";
  String incremental = "00";
  List<int> trama = List<int>();
  String lastTrama = '';
  bool stateDevice = false;
  String messageDevice = '';

  BluetoothCharacteristic characteristic;

  @override
  void initState(){
     WidgetsBinding.instance.addObserver(this);
    super.initState();
    device = widget.device;
    initiliazeConnection();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(state == AppLifecycleState.paused)
    {
      device.disconnect();
    }
  } 

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if(state == BluetoothDeviceState.connected){
      device.disconnect();
    }
  }

  void initiliazeConnection(){
    trama.clear();
    device.state.listen((connectionState) {
      setState(() {
        state = connectionState;
      });
    }).onData((stateData) {
      if(stateData == BluetoothDeviceState.connected){
        Flushbar(
          margin: EdgeInsets.all(8),
          message: "Conexión exitosa",
          icon: Icon(
            Icons.check,
            size: 28.0,
            color: Colors.greenAccent
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.greenAccent
        )..show(context);
        this.completeConfiguration();
      }else{
        Flushbar(
          margin: EdgeInsets.all(8),
          message: "No hay conexión",
          icon: Icon(
            Icons.warning,
            size: 28.0,
            color: Colors.redAccent,
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.redAccent,
        )..show(context);
      }
    });

  }

  Future<void> completeConfiguration()async{
      sharedPreference.thereData(Constants.contadorkey).then((data) {
        print(data);
        if(data){
          sharedPreference.getData(Constants.contadorkey).then((count){
            setState(() {
              print("----------------="+count);
              String clear = count.replaceFirst('"', '');
              List<String> dataSave = clear.split("@");
              if(dataSave[1].contains(device.id.id)){
                statusSync = true;
                contador = dataSave[0];
                print("--------------------" + contador);
              }
              
            });
          });
        }
      });
      List<BluetoothService> services = await device.discoverServices();
      var service = services.firstWhere((service) => service.uuid.toString().contains('0000ffe0'));
      if(service != null){
        var characteristics = service.characteristics;
        characteristic = characteristics.firstWhere((characteristic) => characteristic.uuid.toString().contains('0000ffe1'));
        if(characteristic.isNotifying){
          await characteristic.read();
        }else{
          await characteristic.setNotifyValue(!characteristic.isNotifying);
          await characteristic.read();
        }
        characteristic.value.listen((data){
          onReceiveData(data);
        });
      }
  }

  void onReceiveData(List<int> data) async{
    debugPrint('------------------------------');
    debugPrint('Data recibida: ${data.toString()}');
    debugPrint('Longitud: ${data.length}');

    if(data.length > 0){
      data.forEach((byte) {
        _buffer.add(byte);
      });
      if(_buffer[_buffer.length - 1] == 10 && _buffer[_buffer.length - 2] == 13){
        List<int> bufferRep = []..addAll(_buffer);
        _buffer.clear();
        switch (bufferRep.length) {
          case 4:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("OK")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "OK",
                icon: Icon(
                  Icons.check,
                  size: 28.0,
                  color: Colors.greenAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.greenAccent,
              )..show(context);              
            }
            break;
          case 7:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("ERROR")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);
            }
            break;
          case 11:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("syncupbad")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "SYNC ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);
              sharedPreference.thereData(Constants.contadorkey).then((data) {
                print(data);
                if(data){
                  sharedPreference.removeData(Constants.contadorkey).then((count){
                    print("Borrro la data");
                    setState(() {
                      contador = "";
                      incremental = "";
                      statusSync = false;
                    });
                  });
                }
              });
            }else if(dataTrama.contains("syncup:ok")){
              //Almacena contador
              sharedPreference.saveData(json.encode(contador +"@"+ device.id.id), Constants.contadorkey).then((respond){
                Flushbar(
                margin: EdgeInsets.all(8),
                message: "SYNC OK",
                icon: Icon(
                  Icons.check,
                  size: 28.0,
                  color: Colors.greenAccent
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.greenAccent
              )..show(context);
             });
             setState(() {
               statusSync = true;
             });
              
            }
            break;
          case 36:
            debugPrint('Debo procesar la trama');
            processData(bufferRep);
            break;
          default:
            break;
        }
      }
    }
  }

  void processData(List<int> data) {
    RemoteCoreService corService = new RemoteCoreService(device);
    data = secureService.desencryptTrama(data);
    debugPrint('$data');
    if(data != null){
      setState(() {
        lastTrama = String.fromCharCodes(data);
      });
      switch(data[0]){
      //Data information
        case 36:
          bool state = false;
          String text = "";
          switch(data[12]){
            case 73:
              state = false;
              text = "Puerta Liberada";
              break;
            case 68:
              state = true;
              text = "Puerta Bloqueada";
              break;
            case 72:
              state = false;
              text = "Puerta Abierta";
              break;
            case 67:
              state = false;
              text = "Puerta Cerrada";
              break;
            case 84:
              state = false;
              text = "Actuador Forzado";
              break;
            case 74:
              state = false;
              text = "Actuador Desconectado";
              break;
            case 71:
              state = false;
              text = "Puerta en Riesgo";
              break;
            case 77:
              state = false;
              text = "Motor en Movimiento";
              break;
            case 78:
              state = true;
              text = "Intento de Apertura no Autorizada";
              break;
            case 79:
              state = true;
              text = "Apertura por Boton Interno";
              break;
            case 69:
              state = true;
              text = "Cierre por Boton Interno";
              break;
            default:
              state = false;
              break;
          }
          setState(() {
            stateDevice = state;
            messageDevice = text;
          });
          break;
        //Data Sync
        case 115:
          List<int> dataTrama = corService.syncupTrama(data, 1);
          contador = String.fromCharCode(dataTrama[29]) + String.fromCharCode(dataTrama[30]);
          String incTemp = String.fromCharCode(data[12]) + String.fromCharCode(data[13]);
          int incTmp= int.parse(incTemp);
          incremental = incTmp.toString();
          print("---------------------->" + incremental);
          sendData(secureService.encrypTrama(dataTrama));
          break;
        //Update Incremental
        case 35:
          String incTemp = String.fromCharCode(data[1]) + String.fromCharCode(data[2]);
          int incTmp= int.parse(incTemp) - 3;
          incremental = incTmp.toString();
          Flushbar(
            margin: EdgeInsets.all(8),
            message: 'Incremental actualizado',
            icon: Icon(
              Icons.check,
              size: 28.0,
              color: Colors.greenAccent
            ),
            duration: Duration(seconds: 3),
            leftBarIndicatorColor: Colors.greenAccent
          )..show(context);
          break;
        default:
          Flushbar(
            margin: EdgeInsets.all(8),
            message: "Datos desconocidos",
            icon: Icon(
              Icons.warning,
              size: 28.0,
              color: Colors.redAccent,
            ),
            duration: Duration(seconds: 3),
            leftBarIndicatorColor: Colors.redAccent,
          )..show(context);
          break;
      }
    }
  }

  void _setCommand( bool value, String res, String inc) {
    RemoteCoreService corService = new RemoteCoreService(device);
    List<int> dataTrama = corService.accionTrama(1, value, res, inc);
    incremental = String.fromCharCode(dataTrama[10]) + String.fromCharCode(dataTrama[11]);
    sendData(secureService.encrypTrama(dataTrama));
  }

  void sendData(List<int> data) async{
    await characteristic.write(data).whenComplete((){
      debugPrint('Envie datos =======> $data');
    }).catchError((error) => debugPrint('Error ========> $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(device.name),
        actions: <Widget>[
          StreamBuilder<BluetoothDeviceState>(
            stream: device.state,
            initialData: BluetoothDeviceState.connecting,
            builder: (c, snapshot) {
              VoidCallback onPressed;
              String text;
              switch (snapshot.data) {
                case BluetoothDeviceState.connected:
                  onPressed = () => device.disconnect();
                  text = 'Desconectar';
                  break;
                case BluetoothDeviceState.disconnected:
                  onPressed = (){
                    device.connect();
                    this.initiliazeConnection();
                  };
                  text = 'Conectar';
                  break;
                default:
                  onPressed = null;
                  text = snapshot.data.toString().substring(21).toUpperCase();
                  break;
              }
              return FlatButton(
                  onPressed: onPressed,
                  child: Text(
                    text,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .button
                        .copyWith(color: Colors.white),
                  ));
            },
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              StreamBuilder<BluetoothDeviceState>(
                stream: device.state,
                initialData: BluetoothDeviceState.connecting,
                builder: (c, snapshot) {
                  final state = snapshot.data;
                  debugPrint('$state');
                  switch(state){
                    case BluetoothDeviceState.disconnected:
                      return Align(
                        alignment: Alignment.center,
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                Icons.bluetooth_disabled,
                                size: 200.0,
                                color: Colors.grey,
                              ),
                              Text(
                                'Sin conexion',
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(color: Colors.grey),
                              ),
                            ],
                          ),
                        )
                      );
                      break;
                    case BluetoothDeviceState.connecting:
                      return Align(
                        alignment: Alignment.center,
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 32, bottom: 16),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                  valueColor: new AlwaysStoppedAnimation<Color>(colorPrimary),
                                ),
                              ),
                              Text(
                                'Realizando conexion',
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      );
                      break;
                    case BluetoothDeviceState.connected:
                      return Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                      
                  SizedBox(height: 10,),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.timer,
                                    color: colorAccent,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Contador",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)
                                  ),
                                  Text(contador,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.devices,
                                    color: colorAccent,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Dispositivo",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)
                                  ),
                                  Text(device.id.id,
                                      style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                             color: statusSync ? Colors.greenAccent : Colors.redAccent,
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.sync,
                                    color: Colors.white,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Estado SYNC",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white)
                                  ),
                                  Text(statusSync ? "Sincronizado" : "No Sincronizado",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  InkWell(
                    onTap: (){
                      if(statusSync){
                          _setCommand(stateDevice, contador, incremental);
                      }else{
                        Flushbar(
                          margin: EdgeInsets.all(8),
                          message: "Unidad no sincronizada",
                          icon: Icon(
                          Icons.info,
                          size: 28.0,
                          color: Colors.orangeAccent,
                          ),
                          duration: Duration(seconds: 2),
                          leftBarIndicatorColor: Colors.orangeAccent,
                        )..show(context);
                      }
                    },
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              stateDevice? Icons.lock : Icons.lock_open,
                              color: stateDevice ? Colors.greenAccent : Colors.redAccent,
                              size: 120.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(messageDevice, style: TextStyle(fontSize: 20.0, color: Colors.grey),),
                            SizedBox(
                              height: 10.0,
                            ),
                          ],
                        ),
                      )
                    ),
                  ),
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: ListTile(
                      
                      title: Text('Ultima Trama valida',style: TextStyle(
                          color: Colors.black
                      ),),
                      subtitle: Text(lastTrama,style: TextStyle(
                          color: Colors.grey
                      ),),
                    ),
                  ),       
                  ],
                        ),
                      );
                      break;
                    case BluetoothDeviceState.disconnecting:
                      return Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Desconectando.....'),
                            Container(
                                margin: EdgeInsets.only(top: 32, bottom: 16),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                  valueColor: new AlwaysStoppedAnimation<Color>(colorPrimary),
                                ),
                            ),
                          ],
                        ),
                      );
                      break;
                    default:
                      return Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Validando Estado.....'),
                            Container(
                                margin: EdgeInsets.only(top: 32, bottom: 16),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                  valueColor: new AlwaysStoppedAnimation<Color>(colorPrimary),
                                ),
                            ),
                          ],
                        ),
                      );
                      break;
                  }
                }
              )
            ],
          ),
        )
      )
    );
  }



}