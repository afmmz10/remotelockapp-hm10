import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remotelockapp/src/core/resources/IFirebaseAuthService.dart';
import 'package:remotelockapp/src/resources/StorageService.dart';
import 'package:remotelockapp/src/ui/Welcome/Welcome.dart';
import 'package:remotelockapp/src/ui/onboard/onBoard.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'package:remotelockapp/src/utils/const.dart';
import 'package:google_fonts/google_fonts.dart';

class App extends StatelessWidget {

  App({Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return MaterialApp(
        theme: ThemeData(
          primaryColor: colorPrimary,
          accentColor: colorAccent,
          textTheme:GoogleFonts.latoTextTheme(textTheme).copyWith(
            bodyText1: GoogleFonts.montserrat(textStyle: textTheme.bodyText1),
          ),
        ),
        home: StorageUtil.getBool(Constants.onBoardIntro) ? WelcomePage() : OnBoardingPage()
      );
  }
}