import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:remotelockapp/src/ui/ble/views/ble_find.dart';
import 'package:remotelockapp/src/ui/ble/views/ble_off.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class BleScreen extends StatefulWidget {
  final String user;
  BleScreen({Key key, this.user}) : super(key: key);
  _BleScreenState createState() => _BleScreenState();
}

class _BleScreenState extends State<BleScreen> {

  


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async{
    debugPrint('estado -> ${state.toString()}');
  }


  get dashBg => Column(
    children: <Widget>[
      Expanded(
        child: Container(color: colorAccent),
        flex: 2,
      ),
      Expanded(
        child: Container(color: Colors.transparent),
        flex: 5,
      ),
    ],
  );


  get contentBLE => Container(
    height: MediaQuery.of(context).size.height - 100,
    child: StreamBuilder<BluetoothState>(
      stream: FlutterBlue.instance.state,
      initialData: BluetoothState.unknown,
      builder: (c, snapshot) {
        final state = snapshot.data;
        if (state == BluetoothState.on) {
          return FindDevicesScreen();
        }
        return BluetoothOffScreen(state: state);
      }),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
                dashBg,
                contentBLE
            ],
          ),
        )
    );
  }
}
