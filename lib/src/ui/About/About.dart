import 'package:flutter/material.dart';
import 'package:remotelockapp/src/utils/Colors.dart';

class About extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          "Acerca de",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: colorAccent,
      ),
      body: Stack(
        children: <Widget>[dashBg, content],
      ),
    );
  }

   get dashBg => Column(
    children: <Widget>[
      Expanded(
        child: Container(color: colorAccent),
        flex: 2,
      ),
      Expanded(
        child: Container(color: Colors.transparent),
        flex: 5,
      ),
    ],
  );

  get content => Container(
    child: Column(
      children: <Widget>[
        grid
      ],
    ),
  );

  get grid => Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child:Image(
                      image: AssetImage('assets/images/Logo-app.jpg'),
                    ),
                  ),
                  title: Text('RemoteLock',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('v1.2.0 BETA',style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                  ),),
                  onTap: () {
                    print('horse');
                  },
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.update, color: Colors.white,),
                  ),
                  title: Text('Actualizaciones',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('Verificar actualizaciones',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: colorPrimary,
                    child: Icon(Icons.star, color: Colors.white,),
                  ),
                  title: Text('Calificanos',style: TextStyle(
                    color: Colors.black
                  ),),
                  subtitle: Text('',style: TextStyle(
                    color: Colors.grey
                  ),),
                  onTap: () {
                    print('horse');
                  },
                  selected: true,
                ),
              ),
            ],
          )
        ),
      );

  

}