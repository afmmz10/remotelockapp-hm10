/*import 'dart:async';
import 'dart:typed_data';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:remotelockapp/src/models/deviceHistory.dart';
import 'package:remotelockapp/src/resources/RemoteCoreService.dart';
import 'package:remotelockapp/src/resources/RemoteSecureService.dart';
import 'package:remotelockapp/src/utils/Colors.dart';
import 'package:remotelockapp/src/utils/const.dart';
import 'package:rxdart/rxdart.dart';

class BleAdmin extends StatefulWidget {
  final ScanResult device;
  BleAdmin({Key key, this.device}) : super(key: key);
  _BleAdminState createState() => _BleAdminState();
}

class _BleAdminState extends State<BleAdmin> {

  BleManager _bleManager;
  BehaviorSubject<PeripheralConnectionState> _connectionStateController;
  ValueStream<PeripheralConnectionState> get connectionState => _connectionStateController.stream;
  StreamSubscription monitoringStreamSubscription;

  List<int> _buffer = [];
  String contador = "00";
  String incremental = "00";
  RemoteSecureService secureService = new RemoteSecureService();
  DeviceHistory deviceLog;
  Characteristic chaBle;  
  bool stateSync = false;

  @override
  void initState() {
    _bleManager = BleManager();
    _connectionStateController = BehaviorSubject<PeripheralConnectionState>.seeded(PeripheralConnectionState.disconnected);
    super.initState();
    initiliazeConnection();
  }

  @override
  void dispose()  {
    disconnect();
    super.dispose();
  }

  Future<void> initiliazeConnection() async{
    Peripheral peripheral = widget.device.peripheral;
    bool isConnected = false;
    peripheral.isConnected().then((value) => value == true);
    peripheral.observeConnectionState(emitCurrentValue: true, completeOnDisconnect: true).listen((connectionState) {
      _connectionStateController.add(connectionState);
    });    
    try {
      print("Connecting to ${peripheral.name}");
      if(isConnected){
        this.completeConfiguration();
      }else{
        await peripheral.connect().whenComplete(() => this.completeConfiguration());
        print("Connected!");  
      }
      
    } on BleError catch (e) {
      print(e.toString());
    }

  }

  Future<void> disconnect() async {
    if (await widget.device.peripheral.isConnected()) {
      print("DISCONNECTING...");
      await widget.device.peripheral.disconnectOrCancelConnection();
    }
    await _connectionStateController.drain();
    _connectionStateController.close();
    print("Disconnected!");
  }

  Future<void> completeConfiguration() async {
    await widget.device.peripheral.discoverAllServicesAndCharacteristics();
    List<Service> services = await widget.device.peripheral.services();
    var service = services.firstWhere((service) => service.uuid.contains('0000ffe0'));
    if(service != null){
        List<Characteristic> characteristics = await service.characteristics();
        var characteristic = characteristics.firstWhere((characteristic) => characteristic.uuid.contains('0000ffe1'));
        chaBle = characteristic;
        _startMonitoringData(characteristic.monitor(transactionId: 'monitor'));
    }else{
      Flushbar(
        margin: EdgeInsets.all(8),
        message: "Servicio no encontrado",
        icon: Icon(
          Icons.warning,
          size: 28.0,
          color: Colors.orangeAccent,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.orangeAccent,
      )..show(context);
    }
  }

  void _startMonitoringData(Stream<Uint8List> characteristicUpdates) async {
      await monitoringStreamSubscription?.cancel();
      monitoringStreamSubscription = characteristicUpdates.listen((data) {
        onReceiveData(data);
      },
      onError: (error) {
        print("Error while monitoring characteristic \n$error");
      },
      cancelOnError: true,
    );
  }

  void onReceiveData(List<int> data) async{
    debugPrint('Data recibida: ${data.toString()}');
    debugPrint('Longitud: ${data.length}');

    if(data.length > 0){
      data.forEach((byte) {
        _buffer.add(byte);
      });
      if(_buffer[_buffer.length - 1] == 10 && _buffer[_buffer.length - 2] == 13){
        List<int> bufferRep = []..addAll(_buffer);
        _buffer.clear();
        switch (bufferRep.length) {
          case 4:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("OK")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "OK",
                icon: Icon(
                  Icons.check,
                  size: 28.0,
                  color: Colors.greenAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.greenAccent,
              )..show(context);              
            }
            break;
          case 7:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("ERROR")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);
            }
            break;
          case 11:
            String dataTrama = String.fromCharCodes(bufferRep);
            if(dataTrama.contains("syncupbad")){
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "SYNC ERROR",
                icon: Icon(
                  Icons.warning,
                  size: 28.0,
                  color: Colors.redAccent,
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.redAccent,
              )..show(context);
            }else if(dataTrama.contains("syncup:ok")){
              //Almacena contador
              setState(() {
                stateSync = true;
              });
              Flushbar(
                margin: EdgeInsets.all(8),
                message: "SYNC OK",
                icon: Icon(
                  Icons.check,
                  size: 28.0,
                  color: Colors.greenAccent
                ),
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: Colors.greenAccent
              )..show(context);
            }
            break;
          case 36:
            debugPrint('Debo procesar la trama');
            processData(bufferRep);
            break;
          default:
            break;
        }
      }
    }
  }

  void processData(List<int> data) {
    print(data.length);
    RemoteCoreService corService = new RemoteCoreService(widget.device.peripheral);
    data = secureService.desencryptTrama(data);
    if(data != null){
      print("Llegaron datos");
      print(data);
      switch(data[0]){
      //Data information
        case 36:
          bool state = false;
          String text = "";
          switch(data[12]){
            case 73:
              state = false;
              text = "Puerta Liberada";
              break;
            case 68:
              state = true;
              text = "Puerta Bloqueada";
              break;
            case 72:
              state = false;
              text = "Puerta Abierta";
              break;
            case 67:
              state = false;
              text = "Puerta Cerrada";
              break;
            case 84:
              state = false;
              text = "Actuador Forzado";
              break;
            case 74:
              state = false;
              text = "Actuador Desconectado";
              break;
            case 71:
              state = false;
              text = "Puerta en Riesgo";
              break;
            case 77:
              state = false;
              text = "Motor en Movimiento";
              break;
            case 78:
              state = true;
              text = "Intento de Apertura no Autorizada";
              break;
            case 79:
              state = true;
              text = "Apertura por Boton Interno";
              break;
            case 69:
              state = true;
              text = "Cierre por Boton Interno";
              break;
            default:
              state = false;
              break;
          }
          DeviceHistory dLog = new DeviceHistory(
              id: widget.device.peripheral.identifier,
              contador: String.fromCharCode(data[29]) + String.fromCharCode(data[30]),
              position: String.fromCharCode(data[10]),
              lastTrama: data.toString(),
              stateText: text,
              status: state.toString()
          );
          setState(() {
            deviceLog = dLog;
          });
          break;
        //Data Sync
        case 115:
          List<int> dataTrama = corService.syncupTrama(data, 1);
          contador = String.fromCharCode(dataTrama[29]) + String.fromCharCode(dataTrama[30]);
          String incTemp = String.fromCharCode(data[12]) + String.fromCharCode(data[13]);
          int incTmp= int.parse(incTemp);
          incremental = incTmp.toString();
          print("---------------------->" + incremental);
          sendDataBLE(secureService.encrypTrama(dataTrama));
          break;
        //Update Incremental
        case 35:
          break;
        default:
          Flushbar(
            margin: EdgeInsets.all(8),
            message: "Datos desconocidos",
            icon: Icon(
              Icons.warning,
              size: 28.0,
              color: Colors.redAccent,
            ),
            duration: Duration(seconds: 3),
            leftBarIndicatorColor: Colors.redAccent,
          )..show(context);
          break;
      }
    }else{
      Flushbar(
        margin: EdgeInsets.all(8),
        message: "Error desencriptando trama",
        icon: Icon(
          Icons.warning,
          size: 28.0,
          color: Colors.redAccent,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.redAccent,
      )..show(context);
    }
  }

  Future<void> sendDataBLE(Uint8List dataSend) async {
    print(chaBle);
    await chaBle.write(dataSend, true, transactionId: 'characteristicWrite').whenComplete((){
      Flushbar(
        margin: EdgeInsets.all(8),
        message: "Datos enviados",
        icon: Icon(
        Icons.info,
        size: 28.0,
        color: Colors.blueAccent,
        ),
        duration: Duration(seconds: 2),
        leftBarIndicatorColor: Colors.blueAccent,
      )..show(context);
    }).catchError((error) {
      print("Error========> ${error}");
    }).then((value)=> {
      
    });
  }

  get dashBg => Column(
    children: <Widget>[
      Expanded(
        child: Container(color: colorAccent),
        flex: 2,
      ),
      Expanded(
        child: Container(color: Colors.transparent),
        flex: 5,
      ),
    ],
  );

  get content => Container(
      child: Column(
        children: <Widget>[
          bleAdmin
        ],
      ),
  );

  get bleAdmin => Padding(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: StreamBuilder<PeripheralConnectionState>(
        stream: _connectionStateController,
        initialData: PeripheralConnectionState.connecting,
        builder: (c, snapshot) {
          final state = snapshot.data;
          debugPrint("state: ${state.toString()}");
          switch (state) {
            case PeripheralConnectionState.connecting:
              return  Column(
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 20.0,
                            ),
                            Icon(
                              Icons.bluetooth_connected,
                              color: Colors.blueAccent,
                              size: 80.0,
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text('Conectando.....',
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              );
              break;
            case PeripheralConnectionState.connected:
              return  Column(
                children: <Widget>[
                  Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.blueAccent,
                        child: Icon(Icons.bluetooth, color: Colors.white,),
                      ),
                      title: Text('Estado de conexión',style: TextStyle(
                          color: Colors.black
                      ),),
                      subtitle: Text('CONECTADO',style: TextStyle(
                          color: Colors.grey
                      ),),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.timer,
                                    color: colorAccent,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Contador",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)
                                  ),
                                  Text(deviceLog.contador == null ? 'NA' : deviceLog.contador,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.devices,
                                    color: colorAccent,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Dispositivo No.",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)
                                  ),
                                  Text(deviceLog.position,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                             color: stateSync ? Colors.greenAccent : Colors.redAccent,
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Icon(
                                    Icons.sync,
                                    color: Colors.white,
                                    size: 40.0,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text("Estado SYNC",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white)
                                  ),
                                  Text(stateSync ? "Sincronizado" : "No Sincronizado",
                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white)
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  InkWell(
                    onTap: (){
                      if(stateSync){

                      }else{
                        Flushbar(
                          margin: EdgeInsets.all(8),
                          message: "Unidad no sincronizada",
                          icon: Icon(
                          Icons.info,
                          size: 28.0,
                          color: Colors.orangeAccent,
                          ),
                          duration: Duration(seconds: 2),
                          leftBarIndicatorColor: Colors.orangeAccent,
                        )..show(context);
                      }
                    },
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              deviceLog.status == "true" ? Icons.lock : Icons.lock_open,
                              color: deviceLog.status == "true" ? Colors.greenAccent : Colors.redAccent,
                              size: 120.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(deviceLog.stateText, style: TextStyle(fontSize: 20.0, color: Colors.grey),),
                            SizedBox(
                              height: 10.0,
                            ),
                          ],
                        ),
                      )
                    ),
                  )
                ],
              );
              break;
            case PeripheralConnectionState.disconnected:
              return  Column(
                children: <Widget>[
                  Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundColor: colorError,
                        child: Icon(Icons.bluetooth_disabled, color: Colors.white,),
                      ),
                      title: Text('Estado de conexión',style: TextStyle(
                          color: Colors.black
                      ),),
                      subtitle: Text('DESCONECTADO',style: TextStyle(
                          color: Colors.grey
                      ),),
                    ),
                  ),
                ],
              );
              break;
            case PeripheralConnectionState.disconnecting:
              return  Column(
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 20.0,
                            ),
                            Icon(
                              Icons.bluetooth_disabled,
                              color: colorError,
                              size: 80.0,
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text('Desconectando.....',
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              );
              break;
          }
          return  Column(
            children: <Widget>[

            ],
          );
        }
      ),
  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            widget.device.peripheral.name,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(
              color: Colors.white
          ),
          backgroundColor: colorAccent,
          brightness: Brightness.dark,
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              dashBg,
              content
            ],
          ),
        )
    );
  }
  
}*/