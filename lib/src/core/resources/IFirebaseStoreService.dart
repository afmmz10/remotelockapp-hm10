import 'package:remotelockapp/src/models/deviceEnroll.dart';
import 'package:remotelockapp/src/models/deviceHistory.dart';
import 'package:remotelockapp/src/models/log.dart';

abstract class IFirebaseStoreService {

  Future<void> insertDataLog(Log log);

  Future<bool> setDeviceEnroll(DeviceEnroll deviceEnroll);

  Future<bool> validateDeviceEnroll(String id);

  Future<bool> setDeviceHistory(DeviceHistory deviceHistory);

  Future<bool> updateDeviceEnroll(DeviceEnroll deviceEnroll, String id);
  
}