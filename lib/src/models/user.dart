import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String userId;
  final String userName;
  final String email;
  final bool isverified;

  User({this.userId, this.userName, this.email, this.isverified});

  @override
  // instead of doing super for equatable, we are doing this.
  List<Object> get props => [this.userId, this.userName, this.email, this.isverified];

}