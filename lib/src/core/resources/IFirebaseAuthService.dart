import '../../models/user.dart';

abstract class IFirebaseAuthService {
  Future<bool> isAuthenticated();

  Future<void> signIn(String email, String password);

  Future<User> getUser();

  Future<void> logout();

}