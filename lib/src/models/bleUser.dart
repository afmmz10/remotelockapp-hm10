import 'package:equatable/equatable.dart';

class BleUser extends Equatable {
  final String idDevice;
  final String nameDevice;
  final String typeDevice;
  final String name;
  final String email;
  final String date;

  BleUser({this.idDevice, this.nameDevice, this.typeDevice,this.name, this.email, this.date});

  @override
  // instead of doing super for equatable, we are doing this.
  List<Object> get props => [this.idDevice, this.nameDevice, this.typeDevice,this.name, this.email, this.date];

}